package com.tsystems.javaschool.tasks.calculator;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class Calculator {
    private static final double EPS = 1e-8;

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty()) {
            return null;
        }
        Queue<String> out = reversePolishNotation(statement);

        return calculator(out);

    }

    private Queue<String> reversePolishNotation(String statement) {
        Deque<String> operationsStack = new LinkedList<>();
        Queue<String> outputQueue = new LinkedList<>();

        for (int i = 0; i < statement.length(); i++) {
            char charInput = statement.charAt(i);
            if (isOperation(charInput)) {
                while (!operationsStack.isEmpty()) {
                    char temp = operationsStack.pollFirst().charAt(0);
                    if (isOperation(temp) && (expressionPriority(charInput) <= expressionPriority(temp))) {
                        outputQueue.add(String.valueOf(temp));
                    } else {
                        operationsStack.addFirst(String.valueOf(temp));
                        break;
                    }
                }
                operationsStack.addFirst(String.valueOf(charInput));
            } else if ('(' == charInput) {
                operationsStack.addFirst(String.valueOf(charInput));
            } else if (')' == charInput) {
                char temp = operationsStack.pollFirst().charAt(0);
                while ('(' != temp) {
                    if (operationsStack.size() < 1) {
                        return new LinkedList<>();
                    }
                    outputQueue.add(String.valueOf(temp));
                    temp = operationsStack.pollFirst().charAt(0);
                }
            } else {
                StringBuilder temp = new StringBuilder();

                while (!isOperation(charInput)
                        && '(' != charInput
                        && ')' != charInput) {
                    temp.append(String.valueOf(charInput));
                    i++;
                    if (i == statement.length()) {
                        break;
                    }
                    charInput = statement.charAt(i);
                }
                outputQueue.add(temp.toString());
                i--;
            }
        }

        while (!operationsStack.isEmpty()) {
            outputQueue.add(operationsStack.pollFirst());
        }
        return outputQueue;
    }

    private String calculator(Queue<String> queue) {
        if (queue.isEmpty()) {
            return null;
        }
        Deque<String> answerStack = new LinkedList<>();
        while (!queue.isEmpty()) {
            String temp = queue.poll();
            if (!isOperation(temp.charAt(0))) {
                if ('(' == temp.charAt(0) || ')' == temp.charAt(0)) {
                    return null;
                }
                answerStack.addFirst(temp);
            } else {
                double second;
                double first;
                try {
                    second = Double.valueOf(answerStack.pollFirst());
                    first = Double.valueOf(answerStack.pollFirst());
                } catch (NumberFormatException | NullPointerException e) {
                    return null;
                }
                switch (temp.charAt(0)) {
                    case '+':
                        first += second;
                        break;
                    case '-':
                        first -= second;
                        break;
                    case '*':
                        first *= second;
                        break;
                    case '/':
                        if (second == 0) {
                            return null;
                        }
                        first /= second;
                        break;
                }
                answerStack.addFirst(String.valueOf(first));
            }

        }
        double temp = Double.valueOf(answerStack.pollFirst());
        if (Math.abs(temp - Math.floor(temp)) > EPS) {
            return String.valueOf(temp);
        }
        return String.valueOf((int) temp);
    }

    private static boolean isOperation(char character) {

        switch (character) {
            case '+':
            case '-':
            case '*':
            case '/':
                return true;
        }
        return false;
    }

    private static byte expressionPriority(char character) {
        switch (character) {
            case '+':
            case '-':
                return 1;
        }
        return 2;
    }
}
