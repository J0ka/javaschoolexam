package com.tsystems.javaschool.tasks.pyramid;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {
    private static final int PYRAMID_MAX_COUNT = 16384;
    private static final double EPS = 1e-6;

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.size() == 0) {
            throw new CannotBuildPyramidException("cannot be empty");
        }
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException("array has an empty element");
        }
        if (inputNumbers.size() > PYRAMID_MAX_COUNT) {
            throw new CannotBuildPyramidException("too big");
        }
        Collections.sort(inputNumbers);

        double tempLevels = (Math.sqrt(1 + 8 * inputNumbers.size()) - 1) / 2;

        if (Math.abs(tempLevels - Math.floor(tempLevels)) > EPS) {
            throw new CannotBuildPyramidException("cannot build");
        }
        int levels = (int) tempLevels;

        int numOnLevel = 2 * levels - 1;
        int[][] arrayPyramid = new int[levels][numOnLevel];
        int levelStep = 1;
        int count = 0;
        for (int j = levels - 1; j >= 0; j--) {
            int stepInLevel = 0;
            for (int step = 0; step < levelStep; step++) {
                arrayPyramid[levels - j - 1][j + stepInLevel] = inputNumbers.get(count++);
                stepInLevel += 2;
            }
            levelStep++;
        }

        return arrayPyramid;
    }


}
